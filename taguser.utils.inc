<?php
/**
 * @file taguser.utils.inc
 * General private utility functions for the Tag User module.
 *
 */

/**
 * Delete this user from any content s/he has been tagged in and their tagging
 * preferences.
 *
 * @param $uid The ID of the user being deleted.
 *
 * @return None.
 */
function _taguser_user_delete($uid) {
  $query = "DELETE FROM {taguser} WHERE `tag_uid` = %d";
  db_query($query, array($uid));

  $query = "DELETE FROM {taguser_permission} WHERE `uid` = %d";
  db_query($query, array($uid));
}



/**
 * Remove all user tags for the node that is being deleted.
 *
 * @param $nid The ID of the node being deleted.
 *
 * @return None.
 */
function _taguser_node_delete($nid) {
  $query = "DELETE FROM {taguser} WHERE `content_nid` = %d";

  db_query($query, array($nid));
}


/**
 * Remove all user tags for a node that is being deleted.
 *
 * @param $vid The ID of the revision being deleted.
 *
 * @return None.
 */
function _taguser_node_delete_revision($vid) {
  $query = "DELETE FROM {taguser} WHERE `content_vid` = %d";

  db_query($query, array($vid));
}


/**
 * Fetch the user IDs that are tagged in the given node revision. Only users
 * who are not blocked are returned.
 *
 * @param $vid The node revision ID to load tagged users for.
 *
 * @return An array of user IDs.
 */
function _taguser_node_load($vid) {
  $query  = "SELECT `tag_uid` FROM {taguser} t INNER JOIN
                                   {users} u ON t.`tag_uid` = u.`uid`
             WHERE t.`content_vid` = %d AND u.`status` = 1
             ORDER BY t.`timestamp` DESC";
  $cursor = db_query($query, array($vid));
  $result = array();

  while ($row = db_fetch_object($cursor)) {
    $result[] = $row->tag_uid;
  }

  return $result;
}


/**
 * Updates tags to be present against a new revision of the node.
 *
 * @param $node
 * @return unknown_type
 */
function _taguser_node_update(&$node) {
  $query = "INSERT INTO {taguser} (`content_nid`, `content_vid`, `tag_uid`,
                                   `tagged_by_uid`, `timestamp`)
            (SELECT `content_nid`, %d, `tag_uid`, `tagged_by_uid`, `timestamp`
             FROM {taguser}
             WHERE `content_vid` = %d)";

  // -- Copy the user tags to the current node revision.
  if (!empty($node->revision)) {
    db_query($query, array($node->vid, $node->old_vid));
  }
}

