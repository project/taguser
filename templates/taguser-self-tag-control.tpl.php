<?php
/**
 * @file taguser-self-tag-controls.tpl.php
 * Provides the control links for allowing a user to tag or untag themselves.
 *
 * Available variables:
 * - $action - A machine-readable string indicating the action that the user
 *   can perform on themselves for this node. This value will be 'tag',
 *   'untag', or 'disabled' if the action available to the user is to tag
 *   themselves, untag themselves, or not availablem respectively.
 * - $action_link - The Drupal menu path that will handle the available user
 *   action.
 * - $action_message - A default message to show the user describing the action
 *   they may take and containing a link to that action (for 'tag' or 'untag').
 * - $tagged_by - The user object for the acciont that tagged the viewing user.
 *   This may be the same as $user if the user tagged themselves. This will
 *   be unset if no tag is made or the user is not logged in.
 * - $timestamp - The UNIX timestamp indicating when the user was tagged. This
 *   will be unset if the user is not tagged or the user is not logged in.
 * - $type - The human-readable name of the content type of the node this
 *   template is being added to.
 *
 * The following variables are provided for contextual information.
 * - $node: Node object the tag information is being rendered for.
 * - $user: The user accessing the node.
 *
 */
?>
<div id="taguser-self-tag-<?php print $node->nid; ?>" class="taguser-self-tag-controls">
  <p class="taguser<?php print $action; ?>"><?php print $action_message; ?></p>
</div>
