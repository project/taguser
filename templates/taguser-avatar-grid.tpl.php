<?php
/**
 * @file taguser-avatar-grid.tpl.php
 * Template to wrap the grid of user avatar images.
 *
 * Available variables:
 * - $avatars: An array of rendered user avatar images for the current node.
 *
 * The following variables are provided for contextual information.
 * - $node: Node object the tag information is being rendered for.
 * - $user: The user accessing the node.
 *
 * @see template_preprocess_taguser_avatar_grid()
 *
 */
?>
<div id="taguser-avatar-grid-<?php print $node->nid; ?>" class="taguser-avatar-grid clear-block">
<?php if (!empty($avatars)): ?>
	<?php foreach ($avatars as $avatar): ?>
	  <?php print $avatar; ?>
	<?php endforeach; ?>
<?php endif; ?>
</div>
