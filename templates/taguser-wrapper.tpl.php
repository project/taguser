<?php
/**
 * @file taguser-wrapper.tpl.php
 * Wraps the Tag User output.
 *
 * Available variables:
 * - $avatar_grid: The grid of linked user profile pictures of users tagged in
 *   this content.
 * - $node_type: The human-readable name of the the node type.
 * - $self_tab_controls: The controls to allow a user to tag or untag themselves.
 * - $tag_users_controls: The controls for tagging additional users.
 *
 * The following variables are provided for contextual information.
 * - $node: Node object the tag information is being rendered for.
 * - $user: The user accessing the node.
 *
 * @see template_preprocess_taguser_wrapper()
 *
 */

?>
<div id="tag-user" class="clear-block">
  <h2>People tagged in this <?php print $node_type; ?></h2>
  <?php if (!empty($avatar_grid)): ?>
    <?php print $avatar_grid; ?>
  <?php endif; ?>
  <?php if (!empty($self_tab_controls)): ?>
    <?php print $self_tab_controls; ?>
  <?php endif; ?>
  <?php if (!empty($tag_users_controls)): ?>
    <?php print $tag_users_controls; ?>
  <?php endif; ?>
</div>
