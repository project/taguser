<?php
/**
 * @file taguser-avatar.tpl.php
 * Template for rendering the individual avatar images.
 *
 * Available variables:
 * - $image_title: the alt/title text for the image.
 * - $image_url: the url of the image to display for this user.
 * - $profile_title: the title text for the profile link
 * - $profile_url: If access to user profiles is allowed, the URL to the user
 *   profile. Otherwise, it is not set.
 * - $username: If the username is to be shown with the avatar, the formatted
 *   username. Otherwise, it is not set.
 *
 * The following variables are provided for contextual information.
 * - $account: the user object being rendered.
 * - $user: The user accessing the node.
 *
 * @see template_preprocess_taguser_user_avatar()
 *
 */

?>
<div class="taguser-avatar-image">
  <?php if (isset($profile_url)): ?>
  <a href="<?php print $profile_url; ?>" title="<?php print $profile_title; ?>">
  <?php endif; ?>
    <img src="<?php print $image_url; ?>" alt="<?php print $image_title; ?>" title="<?php print (isset($profile_url) ? $profile_title : $image_title); ?>" />
    <?php if (isset($username)): ?>
    <span class="taguser-username"><?php print $username; ?></span>
    <?php endif; ?>
  <?php if (isset($profile_url)): ?>
  </a>
  <?php endif; ?>
</div>
