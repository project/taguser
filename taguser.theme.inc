<?php
/**
 * @file taguser.theme.inc
 * Theming functions for the Tag User module.
 *
 */


/**
 * Preprocess the controls for the user to tag or untag themselves.
 *
 * @param $vars The theme variables array. After this function is called, these
 *        keys will be available in the array:
 *        - action: A machine-readable string indicating the action that the
 *          user can perform on themselves for this node. This value will be
 *          'tag', 'untag', or 'disabled' if the action available to the user
 *          is to tag themselves, untag themselves, or not available,
 *          respectively.
 *        - action_link: The Drupal menu path that will handle the available
 *          user action.
 *        - action_message: A default message to show the user describing the
 *          action they may take and containing a link to that action (for
 *          'tag' or 'untag').
 *        - tagged_by: The user object for the acciont that tagged the viewing
 *          user. This may be the same as $user if the user tagged themselves.
 *          This will be unset if no tag is made or the user is not logged in.
 *        - timestamp: The UNIX timestamp indicating when the user was tagged.
 *          This will be unset if the user is not tagged or the user is not
 *          logged in.
 *        - type: The human-readable name of the content type of the node this
 *          template is being added to.
 *
 * @return None.
 */
function template_preprocess_taguser_self_tag_control(&$vars) {
  $node          = $vars['node'];
  $user          = $vars['user'];
  $tagged_by_uid = $vars['tagged_by_uid'];
  $is_tagged     = !empty($tagged_by_uid);
  $timestamp     = $vars['timestamp'];
  $type          = node_get_types('name', $node);

  if (!empty($tagged_by_uid)) {
    $tagged_by = ($tagged_by_uid == $user->uid) ? $user : user_load($tagged_by_uid);
  }

  // -- Contextual content.
  if (($user->uid != 0)) {
    $action = $is_tagged ? 'untag' : 'tag';

    if ($action == 'untag') {
      $link_options = array(
        'attributes' => array(
          'title' => t('Untag yourself from this @type.', array('@type' => $type)),
        ),
      );

      $params = array(
        '@type'  => $type,
        '@date'  => format_date($timestamp, 'custom', t('m/d/Y')),
        '!untag' => l(t('untag yourself'), "node/{$node->nid}/taguser/remove-self", $link_options),
      );

      if ($tagged_by->uid == $user->uid) {
        $message = t('You tagged yourself in this @type on @date. If this is incorrect, you may !untag.', $params);
      }
      else {
        $message = t('You were tagged in this @type on @date. If this is incorrect, you may !untag.', $params);
      }
    }
    else {
      $link_options = array(
        'attributes' => array(
          'title' => t('Tag yourself in this @type.', array('@type' => $type)),
        ),
      );

      $params = array(
        '@type' => $type,
        '!tag'  => l(t('Tag yourself'), "node/{$node->nid}/taguser/add-self", $link_options),
      );

      $message = t('Are you in this @type? !tag to let other users know!', $params);
    }
  }
  else {
    $action  = 'disabled';
    $message = t('You must be logged in to tag or untag yourself.');
  }


  // -- Update and cleanup
  $vars['action']         = $action;
  $vars['action_message'] = $message;
  $vars['type']           = $type;

  if (!empty($tagged_by)) {
    $vars['tagged_by'] = $tagged_by;
  }

  unset($vars['tagged_by_uid']);
}


/**
 * Preprocess the entire user avatar section.
 *
 * @param $vars The theme variables array. After this function is called, these
 *        keys will be available in the array:
 *        - avatar_grid: The grid of linked user profile pictures of users
 *          tagged in this node.
 *        - node_type: The human-readable name of the the node type.
 *        - self_tab_controls: The controls to allow a user to tag or untag
 *          themselves.
 *        - tag_users_controls: The controls for tagging additional users.
 *
 * @return None.
 */
function template_preprocess_taguser_wrapper(&$vars) {
  $content           = $vars['content'];
  $vars['node']      = $vars['content']['node'];
  $vars['node_type'] = node_get_types('name', $vars['node']);
  unset($vars['content']);

  $user = $vars['user'];
  $node = $vars['node'];

  // -- Can the user tag or untag themself from content?
  if (user_access('tag self in content') || user_access('untag self from content')) {
    $tagged = taguser_get_user_tag($node->vid, $user->uid);
    $vars['self_tab_controls'] = theme('taguser_self_tag_control', $node,
                                       $tagged['tagged_by_uid'], $tagged['timestamp']);
  }

  // -- Generate the avatar grid.
  if (user_access('see tagged users')) {
    $vars['avatar_grid'] = theme('taguser_avatar_grid', $node);
  }

  // -- Add other users
  if (user_access('tag others in content')) {
  	$vars['tag_users_controls'] = theme('taguser_tag_users_controls', $node);
  }
}


/**
 * Preprocess the data for rendering the user avatar images.
 *
 * @param $vars The theme variables array. After this function is called, these
 *        keys will be available in the array:
 *        - $image_title: the alt/title text for the image.
 *        - $image_url: the url of the image to display for this user.
 *        - $profile_title: the title text for the profile link
 *        - $profile_url: If access to user profiles is allowed, the URL to the
 *          user profile. Otherwise, it is not set.
 *        - $username: If the username is to be shown with the avatar, the
 *          formatted username. Otherwise, it is not set.
 * @return None.
 */
function template_preprocess_taguser_user_avatar(&$vars) {
	$account     = $vars['account'];
  $size_preset = $vars['size_preset'];
  $username    = theme('taguser_username_format', $account);
  $file        = empty($account->picture) ? TAGUSER_DEF_AVATAR_IMAGE : $account->picture;

  // -- If Image Cache is available, use it to get the correctly sized image.
  if (function_exists('imagecache_create_url')) {
    $image_preset      = (!empty($size_preset)) ? $size_preset :
                         variable_get(TAGUSER_IMAGE_CACHE_PRESET, TAGUSER_DEF_IMAGE_CACHE_PRESET);
    $vars['image_url'] = imagecache_create_url($image_preset, $file);
  }
  else {
    $vars['image_url'] =  url($file);
  }

  $vars['image_title'] = check_plain(t('Profile picture for @user', array('@user' => $username)));

  // -- Profile link information if user has access
  if (user_access('access user profiles')) {
    $vars['profile_title'] = check_plain(t('View profile for @user', array('@user' => $username)));
    $vars['profile_url']   = url('user/' . $account->uid);
  }

  if ($vars['show_username'] == TRUE) {
  	$vars['username'] = $username;
  }

  // -- Cleanup the unused variables
  unset($vars['size_preset']);
  unset($vars['show_username']);
}


/**
 * Preprocess the data needed for the avatar grid.
 *
 * @param $vars The theme variables array. This function adds the key 'avatars'
 *        which contains an array of rendered user avatars.
 *
 * @return None.
 */
function template_preprocess_taguser_avatar_grid(&$vars) {
	$users   = $vars['node']->tagged_users;
	$avatars = array();

	if (count($users) > 0) {
		foreach ($users as $uid) {
	    $user      = user_load($uid);
	    $avatars[] = theme('taguser_avatar_cell', $user);
		}

		$vars['avatars'] = $avatars;
	}
}


/**
 * Theme wrapper for the tag user autocomplete form.
 *
 * @param $node The node the form is attached to.
 *
 * @return The rendered HTML form.
 */
function theme_taguser_tag_users_controls($node) {
  return drupal_get_form('taguser_user_tag_controls_form', $node);
}


/**
 * Theme a matched user for the autocomplete drop down.
 *
 * @param $uid The ID of the user to render.
 *
 * @return A rendered HTML snippet.
 */
function theme_taguser_autocomplete_user_match($uid) {
	$account  = user_load($uid);
  $username = theme('taguser_username_format', $account);
  $file     = empty($account->picture) ? TAGUSER_DEF_AVATAR_IMAGE : $account->picture;

  // -- If Image Cache is available, use it to get the correctly sized image.
  if (function_exists('imagecache_create_url')) {
    $image_url = imagecache_create_url('taguser-24', $file);
  }
  else {
    $image_url = url($file);
  }

  $span_style = "background-image: url({$image_url});";

  $output = array();
  $output[] = '<div class="taguser-autocomplete" style="'. $span_style . '">' . $username . '</div>';

	return implode("\n", $output);
}


/**
 * Theme the avatar and username as it should appear in the table cells on the
 * user profile tabs.
 *
 * @param $account The account to render the avatar.
 *
 * @return The rendered HTML to place in the table cell.
 */
function theme_taguser_table_userlink($account) {
  return theme('taguser_user_avatar', $account, TRUE, 'taguser-24');
}


/**
 * Format the user's name. This implementation is aware of the RealName module
 * and will return the name in the format "Real Name (username)" if a
 * real name is present. Otherwise, the username will be returned.
 *
 * @param $account The account to format the name for.
 *
 * @return A string containing raw text with the account's formatted username.
 *         The return value is passed through check_plain() before being
 *         returned.
 */
function theme_taguser_username_format($account) {
  $output = $account->name;

  if (function_exists('realname_make_name')) {
    $realname = realname_make_name($account);

    if ($realname != $output) {
      $output = "{$realname} ({$output})";
    }
  }

  return check_plain($output);
}


/**
 * Theme a single tagged user for the avatar grid. The div element surrounding
 * the avatar will have these CSS classes:
 * - taguser-avatar-cell
 * - taguser-self (if this avatar is of the authenticated user).
 *
 * @param $account The user account to render.
 *
 * @return The rendered HTML for the avatar cell.
 */
function theme_taguser_avatar_cell($account) {
	global $user;

	$cell_class = ($account->uid === $user->uid) ? 'taguser-avatar-cell taguser-self' :
	                                               'taguser-avatar-cell';

  $output   = array();
  $output[] = '<div class="' . $cell_class . '">';
  $output[] = theme('taguser_user_avatar', $account, FALSE);
  $output[] = '</div>';

  return implode("\n", $output);
}




