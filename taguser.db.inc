<?php
/**
 * @file taguser.db.inc
 * Functions whose sole purpose is to operate on the database tables for
 * the Tag User module.
 *
 */


/**
 * Get the tag permission preferences for this user. This function wraps the
 * underlying SQL statement.
 *
 * @param $uid The ID of the user.
 *
 * @return A value indicating the user's permission preferences. Possible
 *         values are:
 *           * FALSE - The user has not entered a preference. For tagging
 *                 purposes, this implicit permission to allow themselves to
 *                 be tagged by anyone.
 *           * 0 - User does not allow themself to be tagged.
 *           * 1 - User allows themself to be tagged by anyone.
 *           * 2 - User can only be tagged by themself.
 */
function taguser_get_permission($uid) {
  $query  = "SELECT `permission` FROM {taguser_permission}
             WHERE `uid` = %d";

  return db_result(db_query($query, array($uid)));
}


/**
 * Get the tag information for the given user on a node revision.
 *
 * @param $vid The revision ID of the node in question.
 * @param $uid The user ID to check.
 *
 * @return If the user is tagged in the given node, an array containing the
 *         keys tagged_by_uid and timestamp indicating which user tagged the
 *         queried user and when (UNIX timestamp). If the user is not tagged in
 *         the revision, FALSE is returned.
 */
function taguser_get_user_tag($vid, $uid) {
  $query  = "SELECT `tagged_by_uid`, `timestamp` FROM {taguser}
             WHERE `content_vid` = %d AND `tag_uid` = %d";
  $result = db_fetch_array(db_query($query, array($vid, $uid)));

  return $result;
}


/**
 * Get the information for a particular user tag.
 *
 * @param $utid The row ID of the user tag.
 *
 * @return An array containing the keys content_nid, content_vid, tag_uid,
 *         tagged_by_uid, and timestamp if the given row ID existed, otherwise,
 *         FALSE is returned.
 */
function taguser_get_user_tag_by_utid($utid) {
  $query = "SELECT `content_nid`, `content_vid`, `tag_uid`,
                   `tagged_by_uid`, `timestamp`
            FROM {taguser}
            WHERE `utid` = %d";
  $result = db_fetch_array(db_query($query, array($utid)));

  return $result;
}


/**
 * Remove a tag by its row ID. This function wraps the underlying SQL
 * statement.
 *
 * @param $utid The row ID to remove.
 *
 * @return None.
 */
function taguser_remove_by_utid($utid) {
  $query = "DELETE FROM {taguser} WHERE `utid` = %d";

  db_query($query, array($utid));
}


/**
 * Remove all the tags for a user. This functionw raps the underlying SQL
 * statement.
 *
 * @param $uid The user ID.
 *
 * @return None.
 */
function taguser_remove_all_tags($uid) {
  $query = "DELETE FROM {taguser} WHERE `uid` = %d";

  db_query($query, array($uid));
}


/**
 * Remove a tag by its unique ID. This function wraps the underlying SQL
 * statement.
 *
 * @param $utid The row ID of the user tag to remove.
 *
 * @return None.
 */
function taguser_remove_tag($utid) {
  $query = "DELETE FROM {taguser} WHERE utid = %d";

  db_query($query, array($utid));
}


/**
 * Remove the tags for a user that were added by other users.
 *
 * @param $uid The user ID.
 *
 * @return None.
 */
function taguser_remove_tags_by_others($uid) {
  $query = "DELETE FROM {taguser}
            WHERE `tag_uid` = %d AND `tag_uid` != `tagged_by_uid`";

  db_query($query, array($uid));
}


/**
 * Removes a tag for a user on a node revision. This function wraps the
 * underlying SQL statement.
 *
 * @param $uid The ID of the user to untag.
 * @param $vid The node revision ID to untag them from.
 *
 * @return None.
 */
function taguser_untag_user($uid, $vid) {
  $query = "DELETE FROM {taguser} WHERE `tag_uid` = %d AND `content_vid` = %d";

  db_query($query, array($uid, $vid));
}


/**
 * Tags a user on a particular node revision. This function wraps the
 * underlying SQL statement.
 *
 * @param $uid The ID of the user being tagged.
 * @param $tagged_by_uid The ID of the user the tag is attributed to.
 * @param $nid The node ID to tag the user in.
 * @param $vid The node revision ID to tag the user in.
 * @param $timestamp Optional - the UNIX timestamp to place on the tag. If not
 *        given, the current time will be used.
 *
 * @return None.
 */
function taguser_tag_user($uid, $tagged_by_uid, $nid, $vid, $timestamp = NULL) {
  $query = "INSERT INTO {taguser} (`content_nid`, `content_vid`, `tag_uid`,
                                   `tagged_by_uid`, `timestamp`)
            VALUES (%d, %d, %d, %d, %d)";

  $timestamp = ($timestamp === NULL) ? time() : $timestamp;

  db_query($query, array($nid, $vid, $uid, $tagged_by_uid, $timestamp));
}

