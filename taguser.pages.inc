<?php
/**
 * @file taguser.pages.inc
 * Interface pages used by the Tag User module.
 *
 */


/**
 * Build the admin settings form for the module. TODO - implement
 *
 * @return An array of form elements.
 */
function taguser_admin_settings() {
	/* - option to not show avatars for users w/o a userpic?
   * - Feature to allow external user search function for autocomplete
   * - option to force username (rather than use theme_username) - both in text and tooltip.
   * - option to show username beside avatar or not at all.
   * - settings for items per page on user's tag list screens - TAGUSER_TAG_LIST_SIZE
   */
}


/**
 * Generates the "Tags of you" tab. The list will only include content if the
 * user is tagged in the most current revision and only those tags that were
 * made by an enabled user.
 *
 * @param $uid The user ID the page is for.
 *
 * @return The page content.
 */
function taguser_tags_of_you($uid) {
	$limit = variable_get(TAGUSER_TAG_LIST_SIZE, TAGUSER_DEF_TAG_LIST_SIZE);

	// -- Build the table header
  $headers = array(
    0 => array(
      'data'  => t('Title'),
      'field' => 'title',
    ),
    1 => array(
      'data'  => t('Type'),
      'field' => 'type',
    ),
    2 => array(
      'data'  => t('Tagged by'),
      'field' => 'uid',
    ),
    3 => array(
      'data'  => t('When'),
      'field' => 'timestamp',
      'sort'  => 'desc',
    ),
    4 => array(
      'data' => t('Change'),
    ),
  );

  $query = "SELECT tu.`utid`, tu.`timestamp`,
                   n.`nid`, n.`title`, nt.`name` AS `type`,
                   u.`uid`, u.`name`, u.`picture`
            FROM {taguser} tu
                INNER JOIN {node}      n  ON n.`vid` = tu.`content_vid`
                INNER JOIN {node_type} nt ON n.`type` = nt.`type`
                INNER JOIN {users}     u  ON u.`uid` = tu.`tagged_by_uid`
            WHERE tu.`tag_uid` = %d AND
                  u.`status` = 1 " . tablesort_sql($headers);

  $result = pager_query($query, $limit, 0, NULL, array($uid));
  $rows   = array();

  // -- Perform the initial row build.
  while ($row = db_fetch_object($result)) {
    $rows[] = array(
      0 => l($row->title, "node/{$row->nid}"),
      1 => $row->type,
      2 => $row->uid,
      3 => format_date($row->timestamp, 'custom', t('m/d/Y')),
      4 => l(t('Untag'), "taguser/{$row->utid}/remove"),
    );
  }

  // -- Process the user links separately since it requires database queries.
  foreach ($rows as &$row) {
  	$account = user_load($row[2]);
  	$row[2]  = theme('taguser_table_userlink', $account);
  }

  // -- Build the output for the page.
  $output = array();
  $output[] = theme('table', $headers, $rows, array('id' => 'taguser-tags-of-me'));
  $output[] = theme('pager', array(), $limit);

  return implode("\n", $output);
}


/**
 * Generates the "People you've tagged" tab. The list will only include content
 * if the user is tagged in the most current revision and only those tags that
 * were made by an enabled user.
 *
 * @param $uid The user ID the page is for.
 *
 * @return The page content.
 */
function taguser_tags_by_you($uid) {
  $limit = variable_get(TAGUSER_TAG_LIST_SIZE, TAGUSER_DEF_TAG_LIST_SIZE);

  // -- Build the table header
  $headers = array(
    0 => array(
      'data'  => t('Title'),
      'field' => 'title',
    ),
    1 => array(
      'data'  => t('Type'),
      'field' => 'type',
    ),
    2 => array(
      'data'  => t('Tagged who?'),
      'field' => 'uid',
    ),
    3 => array(
      'data'  => t('When?'),
      'field' => 'timestamp',
      'sort'  => 'desc',
    ),
  );

  $query = "SELECT tu.`utid`, tu.`timestamp`,
                   n.`nid`, n.`title`, nt.`name` AS `type`,
                   u.`uid`, u.`name`, u.`picture`
            FROM {taguser} tu
                INNER JOIN {node}      n  ON n.`vid` = tu.`content_vid`
                INNER JOIN {node_type} nt ON n.`type` = nt.`type`
                INNER JOIN {users}     u  ON u.`uid` = tu.`tag_uid`
            WHERE tu.`tagged_by_uid` = %d AND
                  u.`status` = 1 " . tablesort_sql($headers);

  $result = pager_query($query, $limit, 0, NULL, array($uid));
  $rows   = array();

  // -- Perform the initial row build.
  while ($row = db_fetch_object($result)) {
    $rows[] = array(
      0 => l($row->title, "node/{$row->nid}"),
      1 => $row->type,
      2 => $row->uid,
      3 => format_date($row->timestamp, 'custom', t('m/d/Y')),
    );
  }

  // -- Process the user links separately since it requires database queries.
  foreach ($rows as &$row) {
    $account = user_load($row[2]);
    $row[2]  = theme('taguser_table_userlink', $account);
  }

  // -- Build the output for the page.
  $output = array();
  $output[] = theme('table', $headers, $rows, array('id' => 'taguser-tags-by-me'));
  $output[] = theme('pager', array(), $limit);

  return implode("\n", $output);
}

